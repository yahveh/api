//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

var movimientosJSONv2 = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

//GET con un html de respuesta
app.get('/',function (req, res) {
  //res.send("Hola mundo desde Node.js");
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/',function(req, res){
  res.send("Hemos recibimos su petición POST");
})

app.put('/',function(req, res){
  res.send("Hemos recibimos su petición PUT");
})

app.delete('/',function(req, res){
  res.send("Hemos recibimos su petición DELETE");
})

//GET con URL distinto a la raiz
app.get('/Clientes', function (req, res) {
  res.send("Aqui tiene a los clientes");
})

//Enviando un GET con un parámetro
app.get('/Clientes/:idcliente', function (req, res) {
  res.send("Aqui tiene al cliente: "+ req.params.idcliente);
})

//Ejemplo de un GET leyendo un JSON y regresandolo como respuesta
app.get('/v1/Movimientos', function(req, res){
  res.sendfile('movimientosv1.json');
})


//GET leyendo JSON de respuesta por variable.
app.get('/v2/Movimientos', function(req, res){
  res.json(movimientosJSONv2);
})


//GEt que filtra en la respuesta el ID solicitado en la URL
app.get('/v2/Movimientos/:id', function(req, res){
  console.log(req.params.id);
  res.send(movimientosJSONv2[req.params.id-1]);
})


//GET que filtra con 2 parametros en la URL como parte de la URL
app.get('/v2/Movimientos/:id/:nombre', function(req, res){
  console.log(req.params);
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send("recibido");
})


//QUERY, petición GET con parámetros por query, en la URL se colocan con "?"
app.get('/v2/Movimientosq', function(req, res){
  console.log(req.query);
  res.send("Petición con Query recibida y cambiada: "+ req.query);
})
